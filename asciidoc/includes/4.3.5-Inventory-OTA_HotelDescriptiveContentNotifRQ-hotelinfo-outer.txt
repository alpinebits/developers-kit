<OTA_HotelDescriptiveContentNotifRQ 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns="http://www.opentravel.org/OTA/2003/05" 
  xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05
                      OTA_HotelDescriptiveContentNotifRQ.xsd" 
  Version="8.000">

  <HotelDescriptiveContents>
    <HotelDescriptiveContent HotelCode="123" 
	  HotelName="Frangart Inn" AreaID="3181913">

     <HotelInfo>
		<!-- Property type and category -->
		<CategoryCodes> ... </CategoryCodes> 
		
		<!-- Property descriptions, pictures and videos -->
		<Descriptions> ... </Descriptions> 
		
		<!-- 3D position -->
		<Position/> 
		
		<!-- Property amenities --> 
		<Services> ... </Services>
	 </HotelInfo>
	 
	 <!-- Property policies as cancellation, extra charges... -->
	 <Policies>	... </Policies>
	 
	 <!-- Property review aggregate -->
	 <AffiliationInfo> ... </AffiliationInfo>
	 
	 <!-- Property address, email, phone, urls... -->
	 <ContactInfos>	... </ContactInfos>

    </HotelDescriptiveContent>
  </HotelDescriptiveContents>

</OTA_HotelDescriptiveContentNotifRQ>
