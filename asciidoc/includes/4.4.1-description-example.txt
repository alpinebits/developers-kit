<Description Name="title">
    <Text TextFormat="PlainText" Language="en">Wellness Offer</Text>
    <Text TextFormat="PlainText" Language="de">Wellness Angebot</Text>
    <Text TextFormat="PlainText" Language="it">Offerta Wellness</Text>
</Description>
<Description Name="intro">
    <Text TextFormat="PlainText" Language="en">Lorem ipsum EN</Text>
    <Text TextFormat="PlainText" Language="de">Lorem ipsum DE</Text>
    <Text TextFormat="PlainText" Language="it">Lorem ipsum IT</Text>
</Description>
<Description Name="description">
    <Text TextFormat="PlainText" Language="en">Lorem ipsum sit amet EN</Text>
    <Text TextFormat="PlainText" Language="de">Lorem ipsum sit amet DE</Text>
    <Text TextFormat="PlainText" Language="it">Lorem ipsum sit amet IT</Text>
    <Text TextFormat="HTML"      Language="en">Lorem ipsum &lt;br&gt; sit amet EN</Text>
    <Text TextFormat="HTML"      Language="de">Lorem ipsum &lt;br&gt; sit amet DE</Text>
    <Text TextFormat="HTML"      Language="it">Lorem ipsum &lt;br&gt; sit amet IT</Text>
</Description>
