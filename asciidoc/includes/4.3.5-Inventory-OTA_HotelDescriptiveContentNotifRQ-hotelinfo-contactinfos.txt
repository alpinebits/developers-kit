<ContactInfos>
	<ContactInfo Location="6">
		<Addresses>
			<Address Language="it">
				<AddressLine>Via Bolzano 63/A</AddressLine>
				<CityName>Bolzano</CityName>
				<PostalCode>39057</PostalCode>
				<StateProv StateCode="BZ"/>
				<CountryName Code="IT"/>
			</Address>
		</Addresses>
		<Phones>
			<Phone PhoneTechType="1" PhoneNumber="+3903720000000"/>
		</Phones>
		<Emails>
			<Email EmailType="5">info@alpinebits.org</Email>
		</Emails>
		<URLs>
			<URL ID="WEBSITE">https://www.alpinebits.org/</URL>
			<URL ID="FACEBOOK">https://www.facebook.com/alpinebits/</URL>
		</URLs>
	</ContactInfo>
</ContactInfos>
