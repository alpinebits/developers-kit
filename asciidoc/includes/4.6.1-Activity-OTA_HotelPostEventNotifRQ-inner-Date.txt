<Date Start="2018-02-23T09:30:00+02:00" End="2018-02-23T11:30:00+02:00">
    <EndDateWindow LatestDate="2018-02-23T09:30:00+02:00"/>

    <LocationCategories>
        <!-- optional location id -->
        <Location AreaID="1" />
        <Category Language="en">Wellness Area</Category>
        <Category Language="de">Wellnessbereich</Category>
        <Category Language="it">Area wellness</Category>

    </LocationCategories>
</Date>