<Offers>
    <Offer>
        <OfferRules>
            <OfferRule MinAdvancedBookingOffset="P30D">
                <Occupancy AgeQualifyingCode="10" MinAge="16" />
                <Occupancy AgeQualifyingCode="8" MinAge="10" MaxAge="16" />
            </OfferRule>
        </OfferRules>
    </Offer>
</Offers>
