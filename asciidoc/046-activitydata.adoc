[[anchor-4.6]]
=== 4.6 ActivityData

If the [request_param]`action` parameter is `OTA_HotelPostEventNotif:EventReports` the client intends to send information about hotel internal activities made available to their guests.

==== 4.6.1 Exchange of hotel activities

The [request_param]`request` parameter contains an `OTA_HotelPostEventNotifRQ` document.

Here is the global structure of the document:
|===
a[activity_bkg]|
[source,xml]
----
include::includes/4.6.1-Activity-OTA_HotelPostEventNotifRQ-outer.txt[]
----
v|[small]**`samples/ActivityData/ActivityData-OTA_HotelPostEventNotifRQ.xml`** - outer part
|===


Each document contains one [xml_element_name]#EventReports# element which MUST contain at least one and at most 99 [xml_element_name]#EventReport# elements.
Each [xml_element_name]#EventReport# element contains two elements [xml_element_name]#EventSites# and [xml_element_name]#GeneralEventInfo#, both mandatory.

[xml_element_name]#EventSites# requires exactly one [xml_element_name]#EventSite# element.

For the attributes [xml_attribute_name]#HotelCode# and [xml_attribute_name]#HotelName#, inside the [xml_element_name]#EventSite# element, the rules are the same as for room availability notifications (<<anchor-4.1.1,section 4.1.1>>).
While it is possible to send more than one [xml_element_name]#EventReport# element, {AlpineBitsR} requires that all [xml_element_name]#EventReport# elements refer to the same hotel.
Hence exactly one hotel can be dealt with in a single request.

An {AlpineBitsR} server must return an error if it receives a request referring to more than one hotel.

The mandatory [xml_element_name]#Event_ID# element has no content and three attributes, all mandatory:

* [xml_attribute_name]#Type# must have the value `18`.
* [xml_attribute_name]#ID# an identifier of the activity as set by the provider.
* [xml_attribute_name]#ID_Context# the context referring to the ID. Usually it's the name of the provider for the activity.

The pair [xml_attribute_name]#ID#,[xml_attribute_name]#ID_Context# is the unique identifier of the activity: it MUST appear only once in a single message and MUST be used as reference for further synchronization messages.

An {AlpineBitsR} server must return an error if the same activity (i.e. multiple conflicting [xml_element_name]#Event_ID# elements) are sent in the same message.

The [xml_element_name]#GeneralEventInfo# holds the information about the activity. It contains the following attributes:

* [xml_attribute_name]#Type#.
* [xml_attribute_name]#URL#. Optional, a URL where the activity can be accessed in the providing system.
* [xml_attribute_name]#Acronym#. Optional, an acronym of the activity. If specified, it must be comprehensible for humans.

Further, the [xml_element_name]#GeneralEventInfo# element contains the following 4 elements:

* [xml_element_name]#EventContacts#. Optional. It must contain at least one [xml_element_name]#EventContact# sub-element.
* [xml_element_name]#AttendeeInfo#. Optional. See below for explanation.
* [xml_element_name]#Dates#. Optional. It must contain at least one and at most 99 [xml_element_name]#Date# sub-elements.
* [xml_element_name]#Comments#. Mandatory. See below for explanation.


An example of the complete structure of the [xml_element_name]#EventContact# element is the following:

|===
a[activity_bkg]|
[source,xml]
----
include::includes/4.6.1-Activity-OTA_HotelPostEventNotifRQ-inner-EventContact.txt[]
----
v|[small]**`samples/ActivityData/ActivityData-OTA_HotelPostEventNotifRQ.xml`** - inner part
|===

Each [xml_element_name]#EventContact# element describes one contact that pertains to the activity. Its optional attribute [xml_attribute_name]#Role#, due the fact that of not being localized, must be agreed upon by the {AlpineBitsR} partners and is used to transmit the Role of this person with regard to this activity (The same person might have different roles in different activities, for instance the "Guide" in an Hiking activity and "Instructor" in a skiing activity).

Further, the [xml_element_name]#EventContact# element contains the following elements:

* The optional [xml_element_name]#PersonName# element describes the referer person for the activity, surname and first name are sent using the mandatory [xml_element_name]#Surname# element and the optional [xml_element_name]#GivenName# element respectively.
* The optional [xml_element_name]#URL# element with the mandatory [xml_attribute_name]#Type# attribute set to `Image`, contains a link to a picture of the contact.
* The optional [xml_element_name]#EmployeeInfo# element contains in the mandatory [xml_attribute_name]#EmployeeId# is used to assign a unique Identifier to the contact person. This could be useful for further synchronization. (Be aware that this attribute is limited to a maximum length of 16 character)

The optional element [xml_element_name]#AttendeeInfo# contains the [xml_attribute_name]#TotalQuantity# attribute that is used to specify the maximum number of guests that can participate to the activity. The attribute [xml_attribute_name]#PreRegisteredQuantity# contains the number of slots that are already busy, hence if the two attributes hold the same value the activity can be considered "sold out". Please note that {AlpineBitsR} currently does not specify a way to book or reserve a slot for activities.

The [xml_element_name]#Dates# element contains at least one and up to 99 [xml_element_name]#Date# elements, which are the dates when the activity is scheduled. The main structure of the [xml_element_name]#Date# element is the following:

|===
a[activity_bkg]|
[source,xml]
----
include::includes/4.6.1-Activity-OTA_HotelPostEventNotifRQ-inner-Date.txt[]
----
v|[small]**`samples/ActivityData/ActivityData-OTA_HotelPostEventNotifRQ.xml`** - inner part
|===

The attributes [xml_attribute_name]#Start# and [xml_attribute_name]#End# define the beginning and the end of the activity, both may also contain time information if available. The [xml_attribute_name]#Start# attribute is mandatory, the [xml_attribute_name]#End# attribute is optional and its absence means that the event is open-ended.
The optional [xml_element_name]#EndDateWindow# element contains the mandatory [xml_attribute_name]#LatestDate# attribute: the deadline by which guests SHOULD subscribe to the activity. 

The optional [xml_element_name]#LocationCategories# element is used to describe the meeting place / location where the activity is planned to happen.
The optional [xml_element_name]#Location# element allows the exchange of a unique identifier for the meeting place through its mandatory [xml_attribute_name]#AreaID# attribute.
At least one [xml_element_name]#Category# element must be specified and contain the mandatory [xml_attribute_name]#Language# attribute set to a two-letter lowercase language abbreviation according to ISO 639-1. At most *one* [xml_element_name]#Category# element is allowed for each [xml_attribute_name]#Language#. Each [xml_element_name]#Category# element is used to transmit the localized name of the location.
    

The Comment section is the part where descriptive elements are defined.

Here is a sample:

|===
a[activity_bkg]|
[source,xml]
----
include::includes/4.6.1-Activity-OTA_HotelPostEventNotifRQ-inner-Comment.txt[]
----
v|[small]**`samples/ActivityData/ActivityData-OTA_HotelPostEventNotifRQ.xml`** - inner part
|===

Inside the [xml_element_name]#Comments# element there are up to 4 [xml_element_name]#Comment# elements.

The [xml_element_name]#Comment# element with the attribute [xml_attribute_name]#Name# set to `Title` is mandatory and it contains at least one [xml_element_name]#Text# element with the mandatory attribute [xml_attribute_name]#Language# set to a two-letter lowercase language abbreviation according to ISO 639-1.
Multiple [xml_element_name]#Text# elements can be specified, but each must specify a different [xml_attribute_name]#Language#: these are the title of the activity in the different languages.

The [xml_element_name]#Comment# element with the attribute [xml_attribute_name]#Name# equal to `Category` is mandatory and it contains at least one [xml_element_name]#Text# element with the optional attribute [xml_attribute_name]#Language# set to a two-letter lowercase language abbreviation according to ISO 639-1.
Multiple [xml_element_name]#Text# elements can be specified, but each must specify a different [xml_attribute_name]#Language#: These are the categories of the activity in the different languages.
At most one [xml_element_name]#Text# element can be specified without the [xml_attribute_name]#Language# attribute: this holds a unique identifier for the category, not meant for displaying to humans.

The [xml_element_name]#Comment# element with [xml_attribute_name]#Name# equal to `Gallery` is optional and it contains at least one [xml_element_name]#Image# element without attributes. Each [xml_element_name]#Image# element contains a valid url to an image that can be used to display the activity.

The [xml_element_name]#Comment# element with [xml_attribute_name]#Name# equal to `Description` is optional and it contains at least one [xml_element_name]#Text# element with the mandatory attribute [xml_attribute_name]#Language# set to a two-letter lowercase language abbreviation according to ISO 639-1.
Multiple [xml_element_name]#Text# elements can be specified, but each must specify a different [xml_attribute_name]#Language#: These are the descriptions of the activity in the different languages.

==== 4.6.2 ActivityData: Synchronization and deletion

Upon receiving an activity (identified by the given [xml_element_name]#Event_ID#) an {AlpineBitsR} server MUST replace the previous activity stored with the same identificative in its entirety. This means that in case of rescheduling or cancellation of specific dates of an activity the whole activity must be sent again with up to date information.

In case of the complete cancellation of an activity, with all its dates, a client can notify to the server a deletion sending one [xml_element_name]#EventReport# element formed according to the following rules:

* The [xml_element_name]#EventSites# element MUST contain the same data of the original message for new activity.
* the [xml_element_name]#GeneralEventInfo# element MUST be empty (self closed tag).

Here is an example:

|===
a[activity_bkg]|
[source,xml]
----
include::includes/4.6.2-Activity-OTA_HotelPostEventNotifRQ-deletion-inner.txt[]
----
v|[small]**`samples/ActivityData/ActivityData-OTA_HotelPostEventNotifRQ-deletion.xml`** - inner part
|===


[[anchor-4.6.3]]
==== 4.6.3. Server response

The server will send a response indicating the outcome of the request.
The response is a `OTA_HotelPostEventNotifRS` document.
Any of the four possible {AlpineBitsR} server response outcomes (success, advisory, warning or error) are allowed.
See <<anchor-2.3,section 2.3>> for details.

Here is an example:

|===
a[activity_bkg]|
[source,xml]
----
include::includes/4.6.3-Activity-OTA_HotelPostEventNotifRS-success.txt[]
----
v|[small]**`samples/ActivityData/ActivityData-OTA_HotelPostEventNotifRS-success.xml`**
|===

[[anchor-4.7.4]]
==== 4.7.4. Tabular representation of OTA_HotelPostEventNotifRQ
include::tables/OTA_HotelPostEventNotifRQ.adoc[]
link:file:///files/schema-xsd/alpinebits.xsd[AlpineBits XSD Schema]

[[anchor-4.7.5]]
==== 4.7.5. Tabular representation of OTA_HotelPostEventNotifRS
include::tables/OTA_HotelPostEventNotifRS.adoc[]
link:file:///files/schema-xsd/alpinebits.xsd[AlpineBits XSD Schema]